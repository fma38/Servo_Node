###############################################################################
# Makefile for the project Servo Node
###############################################################################

## General Flags
PROJECT = Servo Node
MCU = attiny841
PROG = usbtiny
TARGET = servo_node.elf
CC = avr-gcc

## Options common to compile, link and assembly rules
COMMON = -mmcu=$(MCU)

## Compile options common for all C compilation units.
CFLAGS = $(COMMON)
#CFLAGS += -Wall -gdwarf-2 -DF_CPU=8000000 -O1 -fsigned-char
CFLAGS += -Wall -gdwarf-2 -DF_CPU=8000000 -Os -mcall-prologues -fsigned-char
CFLAGS += -Wp,-M,-MP,-MT,$(*F).o,-MF,dep/$(@F).d

## Assembly specific flags
ASMFLAGS = $(COMMON)
ASMFLAGS += -x assembler-with-cpp -Wa,-gdwarf2

## Linker flags
LDFLAGS = $(COMMON)
LDFLAGS +=


## Intel Hex file production flags
HEX_FLASH_FLAGS = -R .eeprom

HEX_EEPROM_FLAGS = -j .eeprom
HEX_EEPROM_FLAGS += --set-section-flags=.eeprom="alloc,load"
HEX_EEPROM_FLAGS += --change-section-lma .eeprom=0


## Objects that must be built in order to link
OBJECTS = servo_node.o

## Build
all: $(TARGET) servo_node.hex servo_node.eep size

## Compile
servo_node.o: servo_node.c
	$(CC) $(INCLUDES) $(CFLAGS) -c $<

## Link
$(TARGET): $(OBJECTS)
	 $(CC) $(LDFLAGS) $(OBJECTS) $(LIBDIRS) $(LIBS) -o $(TARGET)

%.hex: $(TARGET)
	avr-objcopy -O ihex $(HEX_FLASH_FLAGS) $< $@

%.eep: $(TARGET)
	avr-objcopy $(HEX_EEPROM_FLAGS) -O ihex $< $@

%.lss: $(TARGET)
	avr-objdump -h -S $< > $@

size: ${TARGET}
	avr-size ${TARGET}

flash: ${TARGET}
	avrdude -c ${PROG} -p ${MCU} -U lfuse:w:0xe2:m -U hfuse:w:0xd6:m -U efuse:w:0xff:m
	avrdude -c ${PROG} -p ${MCU} -U flash:w:servo_node.hex:i

flash_arduino: ${TARGET}
	avrdude -v -c arduino -P /dev/ttyUSB0 -b 57600 -p ${MCU} -U flash:w:servo_node.hex:i

## Clean target
.PHONY: clean
clean:
	-rm -rf $(OBJECTS) servo_node.elf dep/ servo_node.hex servo_node.eep

## Other dependencies
-include $(shell mkdir dep 2>/dev/null) $(wildcard dep/*)
