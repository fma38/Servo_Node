/*
    Servo_Node.ino

    I²C master code for Arduino, to test avr-lib slave code.

    (C) 2019 Frédéric Mantegazza

    Licenced under GPL:

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:

    - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    - Neither the name of the nor the names of its contributors may be used to endorse or
    promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <Wire.h>

const uint8_t I2C_SLAVE_ADDRESS = 0x10;

// Commands
// Must be even values, so they can be used in General Call Address
// 0x04 and 0x06 should not be used.
#define CMD_IDLE  0
#define CMD_MOVE  0x10  // move from current positions to targets, synchronized, using duration
#define CMD_STOP  0x12  // stop current move

// I2C registers
#define REG_CMD         0x00  // R/W, uint8_t
#define REG_INPUTS      0x01  // RO,  uint8_t
#define REG_ADC         0x02  // RO,  uint8_t
#define REG_POSITION_0  0x03  // R/W, uint16_t
#define REG_POSITION_1  0x05  // R/W, uint16_t
#define REG_POSITION_2  0x07  // R/W, uint16_t
#define REG_POSITION_3  0x09  // R/W, uint16_t
#define REG_TARGET_0    0x0b  // R/W, uint16_t
#define REG_TARGET_1    0x0d  // R/W, uint16_t
#define REG_TARGET_2    0x0f  // R/W, uint16_t
#define REG_TARGET_3    0x11  // R/W, uint16_t
#define REG_DURATION    0x13  // R/W, uint16_t
#define REG_ADC_LOW     0x15  // R/W, uint8_t
#define REG_ADC_HIGH    0x16  // R/W, uint8_t
#define REG_STOP_CONFIG 0x17  // R/W, uint8_t
#define REG_STATUS      0x18  // RO,  uint8_t

// Config
#define STOP_CONFIG_INPUT_0      _BV(0)  // 0=disable, 1=enable
#define STOP_CONFIG_EDGE_INPUT_0 _BV(1)  // 0=low-high, 1=high-low
#define STOP_CONFIG_INPUT_1      _BV(2)  // 0=disable, 1=enable
#define STOP_CONFIG_EDGE_INPUT_1 _BV(3)  // 0=low-high, 1=high-low
#define STOP_CONFIG_INPUT_2      _BV(4)  // 0=disable, 1=enable
#define STOP_CONFIG_EDGE_INPUT_2 _BV(5)  // 0=low-high, 1=high-low
#define STOP_CONFIG_ADC          _BV(6)  // 0=disable, 1=enable
#define STOP_CONFIG_EDGE_ADC     _BV(7)  // 0=low-high, 1=high-low

// Status
#define STATUS_UNKNOW_CMD _BV(0)  // unknown command
#define STATUS_MOVING     _BV(1)  // moving
#define STATUS_STOPPED    _BV(2)  // current move has been stopped, either by user or by input/adc


void setup()
{
    Serial.begin(115200);

    Wire.begin();
    TWBR = 12;  // 400kHz ((CPU_FREQ / I2C_FREQ) - 16) / 2)
}


void loop()
{
//    test_inputs();
//    test_blink();
    test_sweep();
}


void test_inputs()
{

    // Read INPUTS register
    Serial.print(millis());
    Serial.print(" - inputs=");
    Serial.println(i2cRead8(I2C_SLAVE_ADDRESS, REG_INPUTS));
    delay(500);
}


void test_blink()
{
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_0, 2000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_1, 2000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_2, 2000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_3, 2000);
    delay(100);

    // Read positions
    Serial.print("pulses=");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_0));
    Serial.print(", ");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_1));
    Serial.print(", ");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_2));
    Serial.print(", ");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_3));
    Serial.println();
    delay(2000);

    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_0, 1000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_1, 1000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_2, 1000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_3, 1000);
    delay(100);

    // Read positions
    Serial.print("pulses=");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_0));
    Serial.print(", ");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_1));
    Serial.print(", ");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_2));
    Serial.print(", ");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_3));
    Serial.println();
    delay(2000);
}


void test_sweep()
{
    Serial.println();
    Serial.println();
    Serial.print("millis=");
    Serial.println(millis());

    // Read INPUTS register
    Serial.print("inputs=");
    Serial.println(i2cRead8(I2C_SLAVE_ADDRESS, REG_INPUTS));
    delay(100);

    // Read ADC register
    Serial.print("adc=");
    Serial.println(i2cRead8(I2C_SLAVE_ADDRESS, REG_ADC));
    delay(100);

    // Set targets, duration and move sync
    Serial.println("Set positions");
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_0, 1000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_1, 1000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_2, 1000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_POSITION_3, 1000);
    delay(1000);

    // Set targets, duration and move sync
    Serial.println("Set targets");
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_TARGET_0, 2000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_TARGET_1, 2000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_TARGET_2, 2000);
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_TARGET_3, 2000);
    Wire.beginTransmission(I2C_SLAVE_ADDRESS);

    Serial.println("Set duration");
    i2cWrite16(I2C_SLAVE_ADDRESS, REG_DURATION, 1000);  //0xbb8);

    Serial.println("Move");
    i2cWrite8(0x00, REG_CMD, CMD_MOVE);  // general address call

    uint8_t status = 0x00;

    // Wait for moving bit set
    Serial.print("Wait for start moving... ");
    do {
        status = i2cRead8(I2C_SLAVE_ADDRESS, REG_STATUS);
        delay(10);
    } while (!(status & STATUS_MOVING));
    Serial.println("ok");

    // Wait for moving bit clear
    Serial.print("Wait for stop moving... ");
    do {
        status = i2cRead8(I2C_SLAVE_ADDRESS, REG_STATUS);
        delay(10);
    } while ((status & STATUS_MOVING));
    Serial.println("ok");
    delay(100);

    // Read positions
    Serial.print("pulses=");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_0));
    Serial.print(", ");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_1));
    Serial.print(", ");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_2));
    Serial.print(", ");
    Serial.print(i2cRead16(I2C_SLAVE_ADDRESS, REG_POSITION_3));
    Serial.println();

    delay(1000);
}


void i2cWrite8(uint8_t address, uint8_t reg, uint8_t value)
{
    Wire.beginTransmission(address);
    Wire.write(reg);
    Wire.write(value);
    Wire.endTransmission(true);
}


uint8_t i2cRead8(uint8_t address, uint8_t reg)
{
    Wire.beginTransmission(address);
    Wire.write(reg);
    Wire.endTransmission(true);
    Wire.requestFrom(address, 1);
    if (Wire.available() == 1) {
        return Wire.read();
    }
}


void i2cWrite16(uint8_t address, uint8_t reg, uint16_t value)
{
    Wire.beginTransmission(address);
    Wire.write(reg);
    Wire.write((uint8_t)(value & 0xff));
    Wire.write((uint8_t)(value >> 8));
    Wire.endTransmission(true);
}


uint16_t i2cRead16(uint8_t address, uint8_t reg)
{
    Wire.beginTransmission(address);
    Wire.write(reg);
    Wire.endTransmission(true);
    Wire.requestFrom(address, 2);
    if (Wire.available() == 2) {
        uint8_t low = Wire.read();
        uint8_t high = Wire.read();
        return (uint16_t)((high << 8) | low);
    }
    else {
        return 42;  // just to check!
    }
}


void i2cWriteBlock(uint8_t address, uint8_t reg, uint8_t* value, size_t nb)
{
    Wire.beginTransmission(address);
    Wire.write(reg);
    for (uint8_t i=0; i<nb; i++) {
        Wire.write(*value++);
    }
    Wire.endTransmission(true);
}


void i2cReadBlock(uint8_t address, uint8_t reg, uint8_t* buff, size_t nb)
{
    Wire.beginTransmission(address);
    Wire.write(reg);
    Wire.endTransmission(true);
    Wire.requestFrom(address, nb);
    if (Wire.available() == nb) {
        for (uint8_t i=0; i<nb; i++) {
            *buff++ = Wire.read();
        }
    }
}
