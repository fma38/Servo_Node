/*
    servo_node.c

    (C) 2019 Frédéric Mantegazza

    I²C handling based on Hisashi Ito code (https://github.com/orangkucing/WireS)

    Licenced under GPL:

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:

    - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    - Neither the name of the nor the names of its contributors may be used to endorse or
    promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Includes
#include <stdint.h>
#include <stddef.h>
#include <avr/io.h>
#include <avr/interrupt.h>
// #include <avr/pgmspace.h>
// #include <util/delay.h>

// Hardware config
#define I2C_SLAVE_ADDRESS 0x10
#define I2C_SLAVE_MASK    0x00
#define I2C_BUFFER_LENGTH    8

#define BIT_INPUT_0  0
#define BIT_INPUT_1  1
#define BIT_INPUT_2  2

#define GET_INPUT_0 (((PINA & _BV(PA0)) >> PA0) << BIT_INPUT_0)
#define GET_INPUT_1 (((PINA & _BV(PA7)) >> PA7) << BIT_INPUT_1)
#define GET_INPUT_2 (((PINB & _BV(PB0)) >> PB0) << BIT_INPUT_2)

#define MIN_PULSE_WIDTH   500
#define MAX_PULSE_WIDTH  2500
#define MAX_TIMER_COUNT 20000  // the timer TOP value (for creating 50Hz)

// Commands
// Must be even values, so they can be used in General Call Address
// 0x04 and 0x06 should not be used.
#define CMD_IDLE  0x00
#define CMD_MOVE  0x10  // move from current positions to targets, synchronized, using duration
#define CMD_STOP  0x12  // stop current move

// I2C registers
#define REG_CMD         0x00  // R/W, uint8_t
#define REG_INPUTS      0x01  // RO,  uint8_t
#define REG_ADC         0x02  // RO,  uint8_t
#define REG_POSITION_0  0x03  // R/W, uint16_t
#define REG_POSITION_1  0x05  // R/W, uint16_t
#define REG_POSITION_2  0x07  // R/W, uint16_t
#define REG_POSITION_3  0x09  // R/W, uint16_t
#define REG_TARGET_0    0x0b  // R/W, uint16_t
#define REG_TARGET_1    0x0d  // R/W, uint16_t
#define REG_TARGET_2    0x0f  // R/W, uint16_t
#define REG_TARGET_3    0x11  // R/W, uint16_t
#define REG_DURATION    0x13  // R/W, uint16_t
#define REG_ADC_LOW     0x15  // R/W, uint8_t
#define REG_ADC_HIGH    0x16  // R/W, uint8_t
#define REG_STOP_CONFIG 0x17  // R/W, uint8_t
#define REG_STATUS      0x18  // RO,  uint8_t

// Config
#define STOP_CONFIG_INPUT_0      _BV(0)  // 0=disable, 1=enable
#define STOP_CONFIG_EDGE_INPUT_0 _BV(1)  // 0=low-high, 1=high-low
#define STOP_CONFIG_INPUT_1      _BV(2)  // 0=disable, 1=enable
#define STOP_CONFIG_EDGE_INPUT_1 _BV(3)  // 0=low-high, 1=high-low
#define STOP_CONFIG_INPUT_2      _BV(4)  // 0=disable, 1=enable
#define STOP_CONFIG_EDGE_INPUT_2 _BV(5)  // 0=low-high, 1=high-low
#define STOP_CONFIG_ADC          _BV(6)  // 0=disable, 1=enable
#define STOP_CONFIG_EDGE_ADC     _BV(7)  // 0=low-high, 1=high-low

// Status
#define STATUS_UNKNOW_CMD _BV(0)  // unknown command
#define STATUS_MOVING     _BV(1)  // moving
#define STATUS_STOPPED    _BV(2)  // current move has been stopped, either by user or by input/adc

// Consts
// S curve (TODO)
// const uint8_t TABLE_S[256] PROGMEM = {
//
// };

// Types
struct i2cStruct
{
    uint8_t buffer[I2C_BUFFER_LENGTH];  // Tx/Rx Buffer         (ISR)
    size_t rxBufferIndex;               // Rx Index             (User & ISR)
    size_t rxBufferLength;              // Rx Length            (ISR)
    size_t txBufferIndex;               // Tx Index             (User & ISR)
    size_t txBufferLength;              // Tx Length            (User & ISR)
    int8_t startCount;                  // repeated START count (User & ISR)
    uint16_t addr;                      // Tx/Rx address        (User & ISR)
};

// Global vars
static volatile struct i2cStruct g_i2c;   // i2c data
static volatile uint8_t g_cmd;            // command
static volatile uint8_t g_register;       // register
static volatile uint8_t g_inputs;         // inputs state
static volatile uint8_t g_adc;            // adc value
static volatile uint8_t g_adcLow;         // for adc threshlod
static volatile uint8_t g_adcHigh;        // for adc threshlod
static volatile uint16_t g_positions[4];  // current positions (pulses widths, in µs)
static volatile uint16_t g_targets[4];    // targets (pulses widths, in µs)
static volatile uint16_t g_duration;      // duration of sync move
static volatile uint8_t g_stopConfig;     // stop conditions
static volatile uint8_t g_status;         // status byte
static volatile uint8_t g_doRefresh;      // 20ms refresh flag


// Init helpers
static void initTimers(void)
{
    // Timer 1
    TCCR1B = _BV(CS11)                 // divider by 8
           | _BV(WGM13) | _BV(WGM12);  // fast PWM (mode 14)

    TCCR1A = _BV(WGM11)    // fast PWM (mode 14)
           | _BV(COM1A1)   // non inverting mode, set OC1A on Compare Match, Clear OC1A at BOTTOM
           | _BV(COM1B1);  // non inverting mode, set OC1B on Compare Match, Clear OC1B at BOTTOM

    // Set TOP for 20ms period
    ICR1 = MAX_TIMER_COUNT - 1;

    // Set pulse width to 0
    OCR1A = 0;
    OCR1B = 0;

    // Timer 2
    TCCR2B = _BV(CS11)                 // divider by 8
           | _BV(WGM23) | _BV(WGM22);  // fast PWM (mode 14)

    TCCR2A = _BV(WGM21)    // fast PWM (mode 14)
           | _BV(COM2A1)   // non inverting mode, set OC2A on Compare Match, Clear OC2A at BOTTOM
           | _BV(COM2B1);  // non inverting mode, set OC2B on Compare Match, Clear OC2B at BOTTOM

    // Set TOP for 20ms period
    ICR2 = MAX_TIMER_COUNT - 1;

    // Set pulse width to 0
    OCR2A = 0;
    OCR2B = 0;

    // Configure mux
    TOCPMSA1 = _BV(TOCC7S0);  // 0C1A routed to TOCC7
    TOCPMSA0 = _BV(TOCC2S0)   // OC1B routed to TOCC2
             | _BV(TOCC1S1)   // OC2A routed to TOCC1
             | _BV(TOCC0S1);  // OC2B routed to TOCC0

//     // Enable outputs
//     TOCPMCOE = _BV(TOCC7OE)   // enable TOCC7
//              | _BV(TOCC2OE)   // enable TOCC2
//              | _BV(TOCC1OE)   // enable TOCC1
//              | _BV(TOCC0OE);  // enable TOCC0

    // Enable Input Capture Interrupt of Timer 1 (for 20ms trigger -> TIMER1_CAPT_vect)
    TIMSK1 = _BV(ICIE1);
}


static void initIO(void)
{
    // Set corresponding TOCCn pins as outputs
    DDRB = _BV(PB2);  // TOCC7
    DDRA = _BV(PA3)   // TOCC2
         | _BV(PA2)   // TOCC1
         | _BV(PA1);  // TOCC0

    // Enable pullup resistors on inputs
    PUEB = _BV(PB0);  // IO0
    PUEA = _BV(PA7)   // IO1
         | _BV(PA0);  // IO2

    // Pin Change Interrupt Enable
    GIMSK = _BV(PCIE1)   // Pin Change Interrupt Enable 1
          | _BV(PCIE0);  // Pin Change Interrupt Enable 0

    // Pin Change Mask Register
    PCMSK1 = _BV(PCINT8);   // PB0
    PCMSK0 = _BV(PCINT7)    // PA7
           | _BV(PCINT0);   // PA0

    // Pull-up unused pins
    PUEA = _BV(PA5);   // MISO

    // Initial inputs read
    g_inputs = GET_INPUT_0
             | GET_INPUT_1
             | GET_INPUT_2;
}


static void initAdc(void)
{
    ADCSRA = _BV(ADEN)    // enable ADC
           | _BV(ADATE)   // Auto Trigger Enable
           | _BV(ADPS1)   // clock divider by 8
           | _BV(ADPS0);  // clock divider by 8

    ADCSRB = _BV(ADLAR);  // result is left adjusted

    // Select ADC10 as input
    ADMUXA = _BV(MUX3)
           | _BV(MUX1);

//     // Select Supply Voltage as reference, gain 1
//     ADMUXB = 0x00;  // not needed

    // Disable digital input on ADC10
    DIDR1 = _BV(ADC10D);

    ADCSRA |= _BV(ADIE)   // enable interrupt
           |  _BV(ADSC);  // start conversion
}


static void initI2C(void)
{
    // Set slave address and mask
    TWSA = (I2C_SLAVE_ADDRESS << 1)  // slave address
         | _BV(0);                   // allow general call address recognition (broadcast)
    TWSAM = I2C_SLAVE_MASK;

    // Init slave data register
    TWSD = 0xFF;

    g_i2c.startCount = -1;

    TWSCRA = _BV(TWSHE)   // TWI SDA Hold Time Enable
           | _BV(TWDIE)   // TWI Data Interrupt Enable
           | _BV(TWASIE)  // TWI Address/Stop Interrupt Enable
           | _BV(TWEN)    // TWI Interface Enable
           | _BV(TWSIE);  // TWI Stop Interrupt Enable
}


// I2C helpers
static int i2cAvailable(void)
{
    return g_i2c.rxBufferLength - g_i2c.rxBufferIndex;
}


static size_t i2cWrite8(uint8_t data)
{
    if (g_i2c.txBufferLength < I2C_BUFFER_LENGTH) {
        g_i2c.buffer[g_i2c.txBufferLength++] = data;
        return 1;
    }
    return 0;
}


static uint8_t i2cRead8(void)
{
    if (g_i2c.rxBufferIndex >= g_i2c.rxBufferLength) {
        return 0;
    }
    return g_i2c.buffer[g_i2c.rxBufferIndex++];
}


static size_t i2cWrite16(uint16_t data)
{
    if ((g_i2c.txBufferLength+1) < I2C_BUFFER_LENGTH) {
        g_i2c.buffer[g_i2c.txBufferLength++] = (uint8_t)(data & 0xff);
        g_i2c.buffer[g_i2c.txBufferLength++] = (uint8_t)((data >> 8) & 0xff);
       return 2;
    }
    return 0;
}


static uint16_t i2cRead16(void)
{
    if ((g_i2c.rxBufferIndex+1) >= g_i2c.rxBufferLength) {
        return 0;
    }
    uint8_t low = g_i2c.buffer[g_i2c.rxBufferIndex++];
    uint8_t high = g_i2c.buffer[g_i2c.rxBufferIndex++];
    return ((uint16_t)high << 8) | low;
}


// static size_t i2cWriteBlock(const uint8_t* data, size_t quantity)
// {
//     if (g_i2c.txBufferLength < I2C_BUFFER_LENGTH) {
//         size_t avail = I2C_BUFFER_LENGTH - g_i2c.txBufferLength;
//         volatile uint8_t* dest = g_i2c.buffer + g_i2c.txBufferLength;
//
//         // Truncate to space avail if needed
//         if (quantity > avail) {
//             quantity = avail;
//         }
//
//         for (size_t count=quantity; count; count--) {
//             *dest++ = *data++;
//         }
//
//         g_i2c.txBufferLength += quantity;
//
//         return quantity;
//     }
//     return 0;
// }
//
//
// static uint16_t i2cReadBlock(uint8_t* data, size_t quantity)
// {
//     if ((g_i2c.rxBufferIndex+quantity) >= g_i2c.rxBufferLength) {
//         return 0;
//     }
//     for (size_t count=quantity; count; count--) {
//         *data++ = g_i2c.buffer[g_i2c.rxBufferIndex++];
//     }
//     return 1;
// }


// I2C callbacks
static uint8_t onI2cAddrReceive(uint16_t addr, uint8_t startCount)
{
    return 1;
}


static void onI2cReceive(size_t nb)
{
    if (nb >= 1) {
        g_register = i2cRead8();
        nb--;

        switch (g_register) {

            case REG_CMD:
                if (nb == 1) {
                    g_cmd = i2cRead8();
                    if ((g_cmd != CMD_MOVE) && (g_cmd != CMD_STOP)) {
                        g_status |= STATUS_UNKNOW_CMD;
                        g_cmd = CMD_IDLE;
                    }
                }
                break;

            case REG_INPUTS:

                // This register is RO
                break;

            case REG_ADC:

                // This register is RO
                break;

            case REG_POSITION_0:
            case REG_POSITION_1:
            case REG_POSITION_2:
            case REG_POSITION_3:
                if (nb == 2) {

                    // Read positions (in µs)
                    uint16_t value = i2cRead16();
                    if (value != 0) {
                        if (value < MIN_PULSE_WIDTH) {
                            value = MIN_PULSE_WIDTH;
                        }
                        if (value > MAX_PULSE_WIDTH) {
                            value = MAX_PULSE_WIDTH;
                        }
                    }

                    // Pulses widths are < 4096, so we can increase computation precision by shifting 4 bits left
                    g_positions[(g_register-REG_POSITION_0)/2] = value << 4;
                }
                break;

            case REG_TARGET_0:
            case REG_TARGET_1:
            case REG_TARGET_2:
            case REG_TARGET_3:
                if (nb == 2) {

                    // Read targets (in µs)
                    uint16_t value = i2cRead16();
                    if (value != 0) {
                        if (value < MIN_PULSE_WIDTH) {
                            value = MIN_PULSE_WIDTH;
                        }
                        if (value > MAX_PULSE_WIDTH) {
                            value = MAX_PULSE_WIDTH;
                        }
                    }

                    // Pulses widths are < 4096, so we can increase computation precision by shifting 4 bits left
                    g_targets[(g_register-REG_TARGET_0)/2] = value << 4;
                }
                break;

            case REG_DURATION:
                if (nb == 2) {

                    // Read duration (in ms)
                    g_duration = i2cRead16();
                }
                break;

            case REG_ADC_LOW:
                if (nb == 1) {

                    // Read ADC low value
                    g_adcLow = i2cRead8();
                }
                break;

            case REG_ADC_HIGH:
                if (nb == 1) {

                    // Read ADC high value
                    g_adcHigh = i2cRead8();
                }
                break;

            case REG_STOP_CONFIG:
                if (nb == 1) {

                    // Read configuration
                    g_stopConfig = i2cRead8();
                }
                break;

            case REG_STATUS:

                // This register is RO
                break;

            default:
                break;
        }

        // Purge buffer
        while (i2cAvailable()) {
            i2cRead8();
        }
    }
}


static void onI2cRequest(void)
{
    switch (g_register) {

        case REG_CMD:
            i2cWrite8(g_cmd);
            break;

        case REG_INPUTS:
            i2cWrite8(g_inputs);
            break;

        case REG_ADC:
            i2cWrite8(g_adc);
            break;

        case REG_POSITION_0:
        case REG_POSITION_1:
        case REG_POSITION_2:
        case REG_POSITION_3:
            i2cWrite16(g_positions[(g_register-REG_POSITION_0)/2] >> 4);
            break;

        case REG_TARGET_0:
        case REG_TARGET_1:
        case REG_TARGET_2:
        case REG_TARGET_3:
            i2cWrite16(g_targets[(g_register-REG_TARGET_0)/2] >> 4);
            break;

        case REG_DURATION:
            i2cWrite16(g_duration);
            break;

        case REG_ADC_LOW:
            i2cWrite8(g_adcLow);
            break;

        case REG_ADC_HIGH:
            i2cWrite8(g_adcHigh);
            break;

        case REG_STOP_CONFIG:
            i2cWrite8(g_stopConfig);
            break;

        case REG_STATUS:
            i2cWrite8(g_status);
            break;

        default:
            break;
    }
}


static void onI2cStop(void)
{
}


// Misc helpers
static void doStop(void)
{
    if (g_status & STATUS_MOVING) {

        // Clear MOVING bit
        g_status &= ~STATUS_MOVING;

        // Set STOP bits
        g_status |= STATUS_STOPPED;
    }

    g_cmd = CMD_IDLE;
}


// I2C interrupt routine
ISR(TWI_SLAVE_vect)
{
    uint8_t i2cStatus = TWSSRA;

    // Bus error or transmit collision
    if ((i2cStatus & (_BV(TWC) | _BV(TWBE)))) {
        g_i2c.startCount = -1;
        TWSSRA |= _BV(TWASIF)  // clear TWI Address/Stop Interrupt Flag
               |  _BV(TWDIF)   // clear TWI Data Interrupt Flag
               |  _BV(TWBE);   // clear TWI Bus Error
        return;
    }

    // Valid address has been received or Stop condition detected
    if (i2cStatus & _BV(TWASIF)) {

        // Valid address has been received (after Start condition)
        if (i2cStatus & _BV(TWAS)) {
            g_i2c.addr = TWSD;
            g_i2c.startCount++;
            if ((g_i2c.addr & 0b11111001) == 0b11110000) {

                // Send ACK
                TWSCRB = _BV(TWHNM)
                       | _BV(TWCMD1)
                       | _BV(TWCMD0);
                return;
            }

            // Call onI2cAddrReceive callback
            g_i2c.rxBufferIndex = 0;
            if (!onI2cAddrReceive(g_i2c.addr, g_i2c.startCount)) {

                // send NACK
                TWSCRB = _BV(TWHNM)
                       | _BV(TWAA)
                       | _BV(TWCMD1)
                       | _BV(TWCMD0);
                return;
            }

            // Master read operation detected
            if ((i2cStatus & _BV(TWDIR))) {
                g_i2c.txBufferLength = 0;

                // Call onI2cRequest callback
                onI2cRequest();  // load Tx buffer with data
                g_i2c.txBufferIndex = 0;
            }

            // Master write operation detected
            else {
                g_i2c.rxBufferLength = 0;
            }
        }

        // Stop condition detected
        else {

            // Master read operation detected
            if ((i2cStatus & _BV(TWDIR))) {

                // Call onI2cStop callback
                onI2cStop();
            }

            // Master write operation detected
            else {

                // Call onI2cReceive callback
                g_i2c.rxBufferIndex = 0;
                onI2cReceive(g_i2c.rxBufferLength);
            }
            g_i2c.startCount = -1;

            TWSSRA = _BV(TWASIF);  // clear interrupt
            return;
        }
    }

    // A data byte has been successfully received
    else if ((i2cStatus & _BV(TWDIF))) {

        // Master read operation detected
        if ((i2cStatus & _BV(TWDIR))) {
            if (g_i2c.txBufferIndex < g_i2c.txBufferLength) {
                TWSD = g_i2c.buffer[g_i2c.txBufferIndex++];
            }

            // Buffer overrun
            else {

                // Wait for any START condition
                TWSCRB = _BV(TWHNM)
                       | _BV(TWCMD1);
                return;
            }
        }

        // Master write operation detected (a data byte has been received)
        else {
            if (g_i2c.rxBufferLength < I2C_BUFFER_LENGTH) {
                g_i2c.buffer[g_i2c.rxBufferLength++] = TWSD;
            }

            // Buffer overrun
            else {

                // send NACK and wait for any START condition
                TWSCRB = _BV(TWHNM)
                       | _BV(TWAA)
                       | _BV(TWCMD1);
                return;
            }
        }
    }

    // Send ACK
    TWSCRB  = _BV(TWHNM)
            | _BV(TWCMD1)
            | _BV(TWCMD0);
}


// IO interrupt routines
ISR(PCINT0_vect)
{
    // Store previous inputs state
    static uint8_t inputsPrev;

    // Clear INPUT_0 and INPUT_1 bits
    g_inputs &= ~_BV(BIT_INPUT_0)
             &  ~_BV(BIT_INPUT_1);

    // Read INPUT_0 and INPUT_1
    g_inputs |= GET_INPUT_0
             |  GET_INPUT_1;

    // Detect edges
    if (g_stopConfig & STOP_CONFIG_INPUT_0) {
        uint8_t senseFalling = g_stopConfig & STOP_CONFIG_EDGE_INPUT_0;
        if (( senseFalling &&  (inputsPrev & _BV(BIT_INPUT_0)) && !(g_inputs & _BV(BIT_INPUT_0))) ||  // falling edge
            (!senseFalling && !(inputsPrev & _BV(BIT_INPUT_0)) &&  (g_inputs & _BV(BIT_INPUT_0)))) {  // rising edge
            doStop();
        }
    }
    if (g_stopConfig & STOP_CONFIG_INPUT_1) {
        uint8_t senseFalling = g_stopConfig & STOP_CONFIG_EDGE_INPUT_1;
        if (( senseFalling &&  (inputsPrev & _BV(BIT_INPUT_1)) && !(g_inputs & _BV(BIT_INPUT_1))) ||  // falling edge
            (!senseFalling && !(inputsPrev & _BV(BIT_INPUT_1)) &&  (g_inputs & _BV(BIT_INPUT_1)))) {  // rising edge
            doStop();
        }
    }

    // Save previous state
    inputsPrev = g_inputs;
}


ISR(PCINT1_vect)
{
    // Store previous inputs state
    static uint8_t inputsPrev;

    // Clear INPUT_2 bit
    g_inputs &= ~_BV(BIT_INPUT_2);

    // Read INPUT_2
    g_inputs |= GET_INPUT_2;

    // Detect edges
    if (g_stopConfig & STOP_CONFIG_INPUT_2) {
        uint8_t senseFalling = g_stopConfig & STOP_CONFIG_EDGE_INPUT_2;
        if (( senseFalling &&  (inputsPrev & _BV(BIT_INPUT_2)) && !(g_inputs & _BV(BIT_INPUT_2))) ||  // falling edge
            (!senseFalling && !(inputsPrev & _BV(BIT_INPUT_2)) &&  (g_inputs & _BV(BIT_INPUT_2)))) {  // rising edge
            doStop();
        }
    }

    // Save previous state
    inputsPrev = g_inputs;
}


// ADC interrupt routine
ISR(ADC_vect)
{
    static uint8_t adcLevelPrev;
    uint8_t adcLevel = adcLevelPrev;

    // Value is left adjusted, we read only high 8 bits
    g_adc = ADCH;

    // Manage hysteresis
    if (g_adc < g_adcLow) {
        adcLevel = 0;
    }
    else if (g_adc > g_adcHigh) {
        adcLevel = 1;
    }

    // Detect edges
    if (g_stopConfig & STOP_CONFIG_ADC) {
        uint8_t senseFalling = g_stopConfig & STOP_CONFIG_EDGE_ADC;
        if (( senseFalling &&  adcLevelPrev && !adcLevel) ||  // falling edge
            (!senseFalling && !adcLevelPrev &&  adcLevel)) {  // rising edge
            doStop();
        }
    }

    // Save previous state
    adcLevelPrev = adcLevel;
}


// Timer interrupt routine (20ms)
ISR(TIMER1_CAPT_vect)
{
    g_doRefresh = 1;
}


// Core
int main(void)
{
    static uint16_t nbSteps;       // nb steps for sync move
    static int16_t increments[4];  // increments for sync move

    // Init hardware
    initTimers();
    initIO();
    initAdc();
    initI2C();

    // Other inits
    g_cmd = CMD_IDLE;
    for (uint8_t servoNum=0; servoNum<4; servoNum++) {
        g_positions[servoNum] = 0;
        g_targets[servoNum] = 0;
    }
    g_duration = 0;
    g_adcLow = 0x40;
    g_adcHigh = 0xc0;
    g_stopConfig = STOP_CONFIG_INPUT_0
                 | STOP_CONFIG_INPUT_1
                 | STOP_CONFIG_INPUT_2
                 | STOP_CONFIG_ADC;
    g_status = 0x00;

    // Enable interrupts
    sei();

    // Main loop
    while (1) {
        switch (g_cmd) {

            case CMD_IDLE:
                break;

            case CMD_MOVE:

                // If not already moving, it means we just received the CMD_MOVE
                // -> compute steps and incs
                if (!(g_status & STATUS_MOVING)) {

                    // Clear STOP bit
                    g_status &= ~STATUS_STOPPED;

                    // Set MOVING bit
                    g_status |= STATUS_MOVING;

                    // Compute steps according to duration (a step is 20ms)
                    // Round using (duration + 10) / 20
                    nbSteps = (g_duration + 10) / 20;
                    if (nbSteps == 0) {
                        nbSteps = 1;
                    }

                    // Compute increments
                    // TODO: use table and implement S curve as well
                    for (uint8_t servoNum=0; servoNum<4; servoNum++) {
                        
                        // If target is not 0 and current position is 0, set position to min
                        if ((g_targets[servoNum] != 0) && (g_positions[servoNum] == 0)) {
                            g_positions[servoNum] = MIN_PULSE_WIDTH << 4;
                        }
                        
                        // If target is 0, do not move
                        if (g_targets[servoNum] == 0) {
                            increments[servoNum] = 0;
                        }
                        else {
                            if (g_targets[servoNum] >= g_positions[servoNum]) {
                                increments[servoNum] = (g_targets[servoNum] - g_positions[servoNum]) / nbSteps;
                            }
                            else {
                                increments[servoNum] = (g_positions[servoNum] - g_targets[servoNum]) / nbSteps;
                                increments[servoNum] = -increments[servoNum];
                            }
                        }
                    }
                }

                // Compute new servos position every 20ms
                if (g_doRefresh) {
                    for (uint8_t servoNum=0; servoNum<4; servoNum++) {
                        g_positions[servoNum] += increments[servoNum];
                    }
                    nbSteps--;
                    g_doRefresh = 0;

                    // If nbSteps reachs 0, move is over
                    if (nbSteps == 0) {
                        for (uint8_t servoNum=0; servoNum<4; servoNum++) {
                            if (g_targets[servoNum] != 0) {
                                g_positions[servoNum] = g_targets[servoNum];
                            }
                        }

                        // Clear MOVING bit
                        g_status &= ~STATUS_MOVING;

                        g_cmd = CMD_IDLE;
                    }
                }

                break;

            case CMD_STOP:
                doStop();
                break;

            default:
                g_cmd = CMD_IDLE;
                break;
        }

        // Refresh pulse widths
        if (g_positions[0] == 0) {
            TOCPMCOE &= ~_BV(TOCC7OE);  // disable TOCC7
        }
        else {
            TOCPMCOE |= _BV(TOCC7OE);  // enable TOCC7
            OCR1A = g_positions[0] >> 4;
        }

        if (g_positions[1] == 0) {
            TOCPMCOE &= ~_BV(TOCC2OE);  // disable TOCC2
        }
        else {
            TOCPMCOE |= _BV(TOCC2OE);  // enable TOCC2
            OCR1B = g_positions[1] >> 4;
        }
        
        if (g_positions[2] == 0) {
            TOCPMCOE &= ~_BV(TOCC1OE);  // disable TOCC1
        }
        else {
            TOCPMCOE |= _BV(TOCC1OE);  // enable TOCC1
            OCR2A = g_positions[2] >> 4;
        }
        
        if (g_positions[3] == 0) {
            TOCPMCOE &= ~_BV(TOCC0OE);  // disable TOCC0
        }
        else {
            TOCPMCOE |= _BV(TOCC0OE);  // enable TOCC0
            OCR2B = g_positions[3] >> 4;
        }
    }

    return 1;
}
